import reflex as rx
from diplomaapp.state import State


def add_participants():
    return rx.box(
        rx.heading("1. Upload participant data file.", size="4"),
        rx.text("It should be a csv or excel file."),
        rx.hstack(
            rx.upload(rx.button("Choose file and upload"),
                      multiple=False,
                      on_drop=State.handle_upload_participant_list(
                rx.upload_files())
            ),
        ),

        rx.heading("2. Delete any header rows, including header titles.",
                   size="4"),
        rx.button("Delete top row",
                  on_click=State.delete_data_row),

        rx.heading("3. Choose name column(s) appropriately (mandatory).",
                   size="4"),
        rx.hstack(
            rx.text("Full name"),
            rx.select(State.data_columns,
                      value=State.full_name_column,
                      on_change=State.set_full_name_column),
        ),
        rx.hstack(
            rx.text("Last name, First name"),
            rx.select(State.data_columns,
                      value=State.last_name_first_name_column,
                      on_change=State.set_last_name_first_name_column),
        ),
        rx.hstack(
            rx.text("First name"),
            rx.select(State.data_columns,
                      value=State.first_name_column,
                      on_change=State.set_first_name_column),
            rx.text("Last name"),
            rx.select(State.data_columns,
                      value=State.last_name_column,
                      on_change=State.set_last_name_column),
        ),

        rx.heading("4. Choose Email column (mandatory).", size="4"),
        rx.hstack(
            rx.text("Email"),
            rx.select(State.data_columns,
                      value=State.email_column,
                      on_change=State.set_email_column),
        ),

        rx.heading("5. Id and Process columns (optional)", size="4"),
        rx.hstack(
            rx.text("Id"),
            rx.select(State.data_columns,
                      value=State.id_column,
                      on_change=State.set_id_column),
        ),

        rx.hstack(
            rx.text("Process"),
            rx.select(State.data_columns,
                      value=State.process_column,
                      on_change=State.set_process_column),
        ),

        rx.heading("6. Apply the column selection", size="4"),
        rx.button("Apply column selection",
                  on_click=State.apply_column_selection),

        rx.heading("7.Replace existing participant data.", size="4"),
        rx.button("Replace and Save",
                  on_click=State.save_processed_participant_data),
        rx.text(State.participant_data_saved_text),

        rx.data_table(
            data=State.participant_file_data,
            resizable=False,
        ),

        margin_right="5%",
        margin_left="5%"
    )
