import reflex as rx
from passlib.context import CryptContext
import logging
from configparser import ConfigParser
from collections import defaultdict
from datetime import datetime
import asyncio

logging.getLogger("passlib").setLevel(logging.ERROR)

config = ConfigParser()
config.read("config_file/auth.config")
c_auth = config["DEFAULT"]
admin_username = c_auth["ADMIN_USERNAME"]
admin_password_hash = c_auth["ADMIN_PASSWORD_HASH"]

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


class RateLimiter:
    def __init__(self, max_attempts, cooling_period):
        """
        init

        Parameters
        ----------
        max_attempts : int, optional
            DESCRIPTION. The default is 10.
        cooling_period : int, optional
            DESCRIPTION. The default is 300 seconds.

        Returns
        -------
        None.

        """
        self.max_attempts = max_attempts
        self.cooling_period = cooling_period
        self.attempts = defaultdict(list)

    def is_allowed(self, key):
        """
        Check if login attempt is allowed for the given key.

        Returns
        -------
            tuple: (bool, int) - (is_allowed, wait_time_in_seconds)
        """
        now = datetime.now()
        self._cleanup_old_attempts(key, now)

        # Get current attempts for the key
        current_attempts = self.attempts[key]

        # If max attempts reached
        if len(current_attempts) >= self.max_attempts:
            oldest_attempt = current_attempts[0]
            time_passed = (now - oldest_attempt).total_seconds()

            # If still in cooling period
            if time_passed < self.cooling_period:
                wait_time = int(self.cooling_period - time_passed)
                return False, wait_time

            # Cooling period passed, reset attempts
            self.attempts[key] = []

        # Add new attempt
        self.attempts[key].append(now)
        return True, 0

    def _cleanup_old_attempts(self, key, now):
        """Remove attempts that are outside the cooling period."""
        if key in self.attempts:
            valid_attempts = [
                attempt for attempt in self.attempts[key]
                if (now - attempt).total_seconds() < self.cooling_period
            ]
            self.attempts[key] = valid_attempts


class AuthState(rx.State):
    is_authenticated: bool = False
    username: str = ""
    password: str = ""
    error_message: str = ""

    _rate_limiter = RateLimiter(max_attempts=int(c_auth["RATELIMIT_MAX_ATTEMPTS"]),
                                cooling_period=int(c_auth["RATELIMIT_COOLING_PERIOD"]))

    _login_time: datetime = 0

    @rx.event
    def login(self):
        """Attempt login with entered credentials."""
        self.error_message = ""

        # check for rate limit
        allowed, wait_time = self._rate_limiter.is_allowed(self.username)
        if not allowed:
            self.error_message = f"Too many attempts. Try again in {wait_time}s"
            return

        # Validate inputs
        if self.username == "" or self.password == "":
            self.error_message = "Please enter both fields"
            return

        # Check credentials
        if self.username != admin_username:
            self.error_message = "Invalid credentials"
            return

        if not pwd_context.verify(self.password, admin_password_hash):
            self.error_message = "Invalid credentials"
            return

        # Successful login
        self.is_authenticated = True

        # Record time for login
        self._login_time = datetime.now()
        yield AuthState.login_timeout()

    @rx.event
    def logout(self):
        """Clear authentication state."""
        self.is_authenticated = False
        self.username = ""
        self.password = ""
        yield rx.redirect("/")

    @rx.event(background=True)
    async def login_timeout(self):
        """Logout after a while."""
        logout_after = int(c_auth["LOGIN_TIMEOUT_TIME"])
        polling_period = int(c_auth["LOGIN_TIMEOUT_POLLING_PERIOD"])
        while True:
            async with self:
                if self.is_authenticated \
                        and (datetime.now() - self._login_time).total_seconds() > logout_after:
                    yield AuthState.logout()

            await asyncio.sleep(polling_period)
