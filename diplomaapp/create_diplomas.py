import reflex as rx

from diplomaapp.state import State


def create_diplomas() -> rx.Component:
    return rx.box(
        rx.button('Create',
                  on_click=State.create_diplomas),

        rx.heading("Log", size="4"),
        rx.html(State.log_messages,
                max_height="10em",
                overflow_y="scroll"),

        rx.heading("Gallery", size="4"),
        rx.hstack(
            rx.box(
                rx.radio(items=State.created_diploma_images_list,
                         value=State.currently_shown_diploma_img,
                         on_change=State.set_currently_shown_diploma_image,
                         direction="column",
                         gap="2",
                         margin_top="2em",
                         ),
                max_height="800px",
                overflow_y="scroll",
            ),
            rx.image(src=State.diploma_gallery_image,
                     width="600px"),
            margin_top="2em",
            justify_content="center",
        ),



        margin="2em"

    )
