import reflex as rx

from diplomaapp.state import State


def diploma_template() -> rx.Component:
    return \
        rx.center(
            rx.vstack(
                rx.hstack(
                    rx.text("Diploma template"),
                    rx.upload(rx.button("Choose file and upload"),
                              multiple=False,
                              accept={
                        "image/png": [".png"],
                        "image/jpeg": [".jpg", ".jpeg"]},
                        on_drop=State.handle_upload_diploma_template(
                            rx.upload_files())
                    ),
                ),

                rx.heading("Participant Name", size="4"),
                rx.hstack(
                    rx.text("Font",
                            width="30em",
                            text_align="right"),
                    rx.select(State.fonts_available,
                              value=State.font_write_diploma_name,
                              on_change=State.set_font_write_diploma_name,
                              width="30em"),
                    justify_content="start"
                ),
                rx.hstack(
                    rx.text("Default Font Size",
                            width="30em",
                            text_align="right"),
                    rx.input(value=State.default_font_size_name,
                             type="number",
                             on_change=State.set_default_font_size_name),
                    justify_content="start"
                ),

                rx.heading("Bottom text (for list of lecturers etc", size="4"),
                rx.hstack(
                    rx.text("Bottom text",
                            width="30em",
                            text_align="right"),
                    rx.input(value=State.bottom_text,
                             on_change=State.set_bottom_text,
                             width="30em"),
                    justify_content="start"
                ),

                rx.heading("Diploma Number", size="4"),
                rx.hstack(
                    rx.text("Include Diploma Number text",
                            width="30em",
                            text_align="right"),
                    rx.checkbox(checked=State.include_dn_text,
                                on_change=State.set_include_dn_text)),
                rx.hstack(
                    rx.text("Diploma number line",
                            width="30em",
                            text_align="right"),
                    rx.input(value=State.diploma_number_line,
                             on_change=State.set_diploma_number_line,
                             width="30em"),
                    justify_content="start"
                ),
                rx.hstack(
                    rx.text("Font",
                            width="30em",
                            text_align="right"),
                    rx.select(State.fonts_available,
                              value=State.font_write_diploma_1,
                              on_change=State.set_font_write_diploma_1,
                              width="30em"),
                    justify_content="start"
                ),
                rx.hstack(
                    rx.text("Font Size",
                            width="30em",
                            text_align="right"),
                    rx.input(value=State.font_size_diploma_number,
                             type="number",
                             on_change=State.set_font_size_diploma_number),
                    justify_content="start"
                ),

                rx.heading(
                    "Determine placement of text on the image", size="4"),
                rx.text("There are two texts that go on the diploma"),
                rx.list(items=["Participant's name.",
                               "Diploma number."]),
                rx.text(
                    "Please move the lines below to figure out the location of the these texts."),
                rx.link("Example",
                        href="/example_diploma_template.jpg",
                        is_external=True),
                rx.grid(
                    rx.card(
                        rx.plotly(data=State.diploma_figure,
                                  layout=State.diploma_figure_layout,
                                  config={'staticPlot': True}),
                        width="1100px"
                    ),

                    rx.card(

                        rx.vstack(
                            rx.text(
                                "Align the red line with the descent line of 'presented to'."),
                            rx.input(id="name-position-top",
                                     type="number",
                                     value=State.name_position_top,
                                     on_change=State.set_name_position_top),

                            rx.text(
                                "Align the blue line with the ascent line of 'for successfully'."),
                            rx.input(id="name-position-bot",
                                     type="number",
                                     value=State.name_position_bot,
                                     on_change=State.set_name_position_bot),

                            rx.text(
                                f'Name position will be the average = {State.text_name_y_position}'),
                            margin_top="450px"
                        ),

                        rx.vstack(
                            rx.text(
                                "Align the magenta line with the descent line of the signatures."),
                            rx.input(id="dn-position-top",
                                     type="number",
                                     value=State.dn_position_top,
                                     on_change=State.set_dn_position_top),

                            rx.text(
                                "Align the gold line with the top of the graphics at the bottom'."),
                            rx.input(id="bot-text-position-bot",
                                     type="number",
                                     value=State.dn_position_bot,
                                     on_change=State.set_dn_position_bot),

                            rx.text(
                                f'Name position will be the average = {State.text_dn_y_position}'),
                            margin_top="150px"
                        ),
                        width="200px",
                    ),
                    id="calc-container",
                    columns="2",
                    rows="1",
                    width="1900px"
                ),
            ),
            margin="2em",
            width="2000px",
        )
