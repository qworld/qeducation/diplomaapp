import reflex as rx

from diplomaapp.auth import AuthState
from diplomaapp.state import State
from diplomaapp.style import Style

from diplomaapp.workshop_details import workshop_details
from diplomaapp.diploma_template import diploma_template
from diplomaapp.email_template import email_template
from diplomaapp.add_participants import add_participants
from diplomaapp.participants import participants
from diplomaapp.create_diplomas import create_diplomas
from diplomaapp.send_diplomas import send_diplomas


def home() -> rx.Component:
    """Create main page."""
    return rx.vstack(
            rx.heading("Diploma Generator " + State.workshop_code,
                       size="8",
                       align="center",
                       width="100%"),
            rx.hstack(
                rx.button("Save config",
                          on_click=lambda: State.create_config(False, False)
                          ),
                rx.text(State.config_save_text),
                ),
            rx.hstack(
                rx.button("Logout",
                          on_click=AuthState.logout
                          ),
                rx.button("Reset",
                          on_click=State.reset_vars)
            ),
            rx.tabs.root(
                rx.tabs.list(
                    rx.tabs.trigger("Workshop details",
                                    value="tab_wd"),
                    rx.tabs.trigger("Diploma Template",
                                    value="tab_dt"),
                    rx.tabs.trigger("Email Template",
                                    value="tab_em"),
                    rx.tabs.trigger("Add Participants",
                                    value="tab_ap"),
                    rx.tabs.trigger("Select Participants",
                                    value="tab_pp"),
                    rx.tabs.trigger("Create diplomas",
                                    value="tab_cd"),
                    rx.tabs.trigger("Send diplomas",
                                    value="tab_sd"),
                    justify_content="center"
                ),
                rx.tabs.content(
                    workshop_details(),
                    value="tab_wd",
                ),
                rx.tabs.content(
                    diploma_template(),
                    value="tab_dt",
                ),
                rx.tabs.content(
                    email_template(),
                    value="tab_em",
                ),
                rx.tabs.content(
                    add_participants(),
                    value="tab_ap"),
                rx.tabs.content(
                    participants(),
                    value="tab_pp",
                ),
                rx.tabs.content(
                    create_diplomas(),
                    value="tab_cd",
                ),
                rx.tabs.content(
                    send_diplomas(),
                    value="tab_sd",
                ),
                default_value="tab_wd",
                width="100%",
            )
        )


def login_form() -> rx.Component:
    """Login form."""
    return rx.center(
        rx.box(
            rx.vstack(
                rx.heading("Secure Login"),
                rx.form(
                    rx.vstack(
                        rx.input(
                            placeholder="Username",
                            on_change=AuthState.set_username,
                            width="100%",
                            autocomplete="username",
                        ),
                        rx.input(
                            type="password",
                            placeholder="Password",
                            on_change=AuthState.set_password,
                            width="100%",
                            autocomplete="current-password",
                        ),
                        rx.button("Login", type_="submit", width="100%"),
                        spacing='3',
                    ),
                    on_submit=AuthState.login,
                ),
                rx.cond(
                    AuthState.error_message != "",
                    rx.text(AuthState.error_message, color="red"),
                ),
                spacing='5',
                align="center",
            ),
            width="100%",
            max_width="320px",
            padding="2em",
            border="1px solid #eaeaea",
            border_radius="8px",
        ),
        height="100vh",
    )


@rx.page(on_load=State.create_workshop_list)
def index() -> rx.Component:
    """Home page."""
    return rx.cond(
        State.is_hydrated,
        rx.cond(
            AuthState.is_authenticated,
            home(),
            rx.vstack(login_form(), align="center"),
        ),
    )


app = rx.App(
        theme=rx.theme(
            appearance="light",
            has_background=True,
            radius="large",
            accent_color="teal",
            ),
        style=Style)


app.add_page(index)
