
email_body = '''<html>
<body>

<p>Hi {name},</p>

<p>Congratulations! You successfully completed the {workshop_code} workshop \
organized by QWorld. Attached with this email is your diploma.</p>

<p>Don't forget to tag @QWorld19 and use the hashtags #QWorld and #{workshop} \
when sharing your diploma on social media.</p>

<p>If you want to know about our future events, please follow us on \
<a href='https://www.facebook.com/qworld19/'>Facebook</a>, \
<a href='https://twitter.com/QWorld19'>Twitter</a>, or \
<a href='https://www.youtube.com/qworld19'>YouTube</a>, \
<a href='http://ej.uz/qworld_discord'>Discord</a>, or \
<a href='https://www.linkedin.com/company/qworld19'>LinkedIn</a>.</p>

<p>Best regards,<br>
QWorld</p> \

<body>\
<html>
'''
