import reflex as rx

from diplomaapp.state import State


def email_template() -> rx.Component:
    return \
        rx.box(
            rx.hstack(
                rx.text('Subject'),
                rx.input(value=State.email_subject,
                         on_change=State.set_email_subject,
                         size="2",
                         width="80em")),

            rx.text('Email body. You can use the following tags:'),
            rx.list.unordered(
                items=["{name} is Participant's full name",
                       "{workshop_code} is something like QSilver5",
                       "{workshop} is something like QSilver"]),
            rx.container(
                rx.editor(
                    set_contents=State.email_content_html,
                    on_change=State.handle_email_editor_change,
                    set_options=rx.EditorOptions(
                        button_list=[["link", "codeView"]],
                    )),
                max_width="80em",
                margin="20px"),
            rx.container(rx.html(State.email_content_html,
                                 ),
                         max_width="70em",
                         margin="20px",
                         padding="20px",
                         ),
            rx.vstack(
                rx.heading("Send a test email",
                           size='6'),
                rx.hstack(
                    rx.text("Test email address",
                            width="30em",
                            text_align="right"),
                    rx.input(value=State.test_email_address,
                             on_change=State.set_test_email_address,
                             width="20em"),
                    justify_content="start"
                ),
                rx.button('Send test email',
                          on_click=State.send_test_email)
            ),
            margin="2em",
        )
