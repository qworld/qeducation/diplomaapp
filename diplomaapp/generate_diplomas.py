"""Provides class to create and send diplomas."""
import os
import csv
from configparser import ConfigParser
from PIL import Image, ImageDraw, ImageFont
import smtplib
import ssl
from email.message import EmailMessage
from email.policy import SMTP
from datetime import datetime, timezone

import logging


class GenerateDiplomas:
    """Class to create and send diplomas."""

    def __init__(self,
                 config_file_path: str,
                 log_file_path: str,
                 ) -> None:

        logging.basicConfig(
            level=logging.DEBUG,
            format='%(message)s',
            handlers=[
                logging.FileHandler(log_file_path),
                logging.StreamHandler()
            ]
        )

        self._attendees = []
        config = ConfigParser()
        config.read(config_file_path, encoding='utf-8')

        self.c = config['DEFAULT']
        self.CSV_FIELD_NAME = self.c['CSV_FIELD_NAME']
        self.CSV_FIELD_EMAIL = self.c['CSV_FIELD_EMAIL']
        self.CSV_FIELD_ID = self.c['CSV_FIELD_ID']
        self.CSV_FIELD_GET_DIPLOMA = self.c['CSV_FIELD_GET_DIPLOMA']

        # compute image width
        img = Image.open(self.c['FILE_DIPLOMA_TEMPLATE'])
        self.width_img = img.width

        # compute text color
        self.text_color = (self.c.getint('TEXT_COLOR_R'),
                           self.c.getint('TEXT_COLOR_G'),
                           self.c.getint('TEXT_COLOR_B'))

        # margins
        min_margin = self.c.getfloat('MIN_MARGIN_PERCENTAGE')
        self.left_margin = self.c.getint('LEFT_MARGIN_SIZE')

        self.width_img_with_margins = self.width_img \
            - self.width_img * min_margin * 2 - self.left_margin

    def generate(self):
        """Generate and/or sends diplomas."""
        # create folders
        os.makedirs(self.c['DIPLOMA_SAVE_PATH'], exist_ok=True)
        os.makedirs(self.c['DIPLOMA_PDF_SAVE_PATH'], exist_ok=True)

        # Login to server if required
        if self.c.getboolean('EMAIL_SENDING'):
            # Create a secure SSL context for sending emails
            context = ssl.create_default_context()

            # Open connection, only once to send all emails
            server = smtplib.SMTP_SSL(self.c['EMAIL_SMTP'],
                                      self.c['EMAIL_PORT'],
                                      context=context)

            server.login(self.c['EMAIL_ACCOUNT'], self.c['EMAIL_PWD'])

        log_msg = 'Loading participant list....'
        logging.debug(log_msg)
        yield log_msg

        with open(self.c['FILE_DATA_CSV'],
                  encoding=self.c['FILE_DATA_ENCODING_OPEN']) as csv_file:
            csv_reader = csv.DictReader(csv_file)

            for attendee in csv_reader:
                # if the attendee diploma should be generated
                if int(attendee[self.CSV_FIELD_GET_DIPLOMA]) == 1:
                    if self.c.getboolean('DIPLOMA_GENERATION'):
                        log_msg = f'Creating diploma for: {attendee[self.CSV_FIELD_NAME]}'
                        logging.debug(log_msg)
                        yield log_msg
                        self._create_diploma(attendee)
                    if self.c.getboolean('EMAIL_SENDING'):
                        log_msg = f'Sending diploma to: {attendee[self.CSV_FIELD_NAME]} ' + \
                                      f'at email: {attendee[self.CSV_FIELD_EMAIL]}'
                        logging.debug(log_msg)
                        yield log_msg
                        self._send_diploma(attendee, server)
                attendee['Processed'] = 'true'
                self._attendees.append(attendee)

        # don't forget to quit
        if self.c.getboolean('EMAIL_SENDING'):
            server.quit()

        log_msg = "Processed all participants."
        logging.debug(log_msg)
        yield log_msg

    def _create_diploma(self, attendee):

        img = Image.open(self.c['FILE_DIPLOMA_TEMPLATE'])
        drw = ImageDraw.Draw(img)

        # draw name
        self._draw_name(attendee, drw)

        # draw diploma number
        if self.c.getboolean('INCLUDE_DN_TEXT'):
            self._draw_bottom_text(attendee, drw)

        # save new diploma
        img.save(os.path.join(self.c['DIPLOMA_SAVE_PATH'],
                              self.c['DIPLOMA_PREFIX_NAME']
                              + attendee[self.CSV_FIELD_ID] + '.jpg'))

        # save as pdf
        img.save(os.path.join(self.c['DIPLOMA_PDF_SAVE_PATH'],
                              self.c['DIPLOMA_PREFIX_NAME']
                              + attendee[self.CSV_FIELD_ID] + '.pdf'))

        attendee['Diploma generated'] = 'true'

    def _draw_name(self, attendee, drw):

        text = attendee[self.CSV_FIELD_NAME]

        font_size = self.c.getint('DEFAULT_FONT_SIZE_NAME')

        # first use default font size
        font = ImageFont.truetype(self.c['FONT_WRITE_DIPLOMA_NAME'], font_size)
        width_txt = drw.textlength(text, font=font)
        # Then reduce font size if name doesn't fit
        while width_txt >= self.width_img_with_margins:
            font_size -= 1
            font = ImageFont.truetype(self.c['FONT_WRITE_DIPLOMA_NAME'], font_size)
            width_txt = drw.textlength(text, font=font)

        location = (self.width_img/2, self.c.getint('TEXT_NAME_Y_POSITION'))

        drw.text(
            location,
            text,
            fill=self.text_color,
            font=font,
            align='center',
            anchor='mm'
            )

    def _draw_bottom_text(self, attendee, drw):

        font_dn = ImageFont.truetype(self.c['FONT_WRITE_DIPLOMA_1'],
                                     self.c.getint('FONT_SIZE_DIPLOMA_NUMBER'))

        text_diploma_number = self.c['TEXT_DIPLOMA_NUMBER'] + \
            str(attendee[self.CSV_FIELD_ID])

        location_diploma_number = (self.width_img/2,
                                   self.c.getint('TEXT_DN_Y_POSITION'))

        bottom_text = self.c['BOTTOM_TEXT']

        if len(bottom_text.strip()) > 0:
            text_diploma_number = bottom_text + '\n' + text_diploma_number

        drw.multiline_text(
            location_diploma_number,
            text_diploma_number,
            fill=self.text_color,
            font=font_dn,
            anchor='mm',
            # spacing=str(int(font_dn/10)),
            align='center'
            )

    def _send_diploma(self, attendee, server):

        # Create message to be sent
        message = EmailMessage(policy=SMTP)
        message['Subject'] = self.c['EMAIL_SUBJECT']
        message['From'] = self.c['EMAIL_ACCOUNT']
        message['To'] = attendee['Email']

        # time
        now = datetime.now(timezone.utc)
        message['Date'] = now.strftime('%a, %d %b %Y %H:%M:%S %z')

        part_plain = self.c['EMAIL_CONTENT_TEXT'].format(
            name=attendee[self.CSV_FIELD_NAME])
        part_html = self.c['EMAIL_CONTENT_HTML'].format(
            name=attendee[self.CSV_FIELD_NAME])

        message.set_content(part_plain)
        message.add_alternative(part_html, subtype='html')

        # attach img
        filename_img = self.c['DIPLOMA_PREFIX_NAME'] + \
            attendee[self.CSV_FIELD_ID] + '.jpg'
        filename_img_with_path = os.path.join(self.c['DIPLOMA_SAVE_PATH'],
                                              filename_img)

        with open(filename_img_with_path, 'rb') as attachment:
            message.add_attachment(attachment.read(), maintype='image',
                                   subtype='jpeg', filename=filename_img)

        # attach pdf
        filename_pdf = self.c['DIPLOMA_PREFIX_NAME'] + \
            attendee[self.CSV_FIELD_ID] + '.pdf'
        filename_pdf_with_path = os.path.join(self.c['DIPLOMA_PDF_SAVE_PATH'],
                                              filename_pdf)

        with open(filename_pdf_with_path, 'rb') as attachment:
            message.add_attachment(attachment.read(), maintype='application',
                                   subtype='pdf', filename=filename_pdf)

        # Send email
        server.send_message(message,
                            from_addr=self.c['EMAIL_ACCOUNT'],
                            to_addrs=attendee[self.CSV_FIELD_EMAIL])

        attendee['Diploma sent'] = 'true'
