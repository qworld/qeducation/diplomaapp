"""Participant data page."""
import reflex as rx
from diplomaapp.state import State


def participants() -> rx.Component:
    """Participant data page."""
    return rx.box(
        rx.box(rx.button("Download saved data",
                         on_click=State.download_participant_data),
               margin_left="10em",
               margin_bottom="1em"),

        rx.container(
            rx.data_editor(
                columns=State.participant_columns,
                data=State.participants,
                on_cell_edited=State.set_participant_details,
                on_header_clicked=State.header_clicked,
            ),

            rx.button("Add participant",
                      on_click=State.add_participant,
                      margin_top="2em"),
        ),

        margin="5%"

    )
