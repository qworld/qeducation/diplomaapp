import reflex as rx

from diplomaapp.state import State


def send_diplomas() -> rx.Component:
    return rx.box(
        rx.button('Send',
                  on_click=State.send_emails),

        rx.heading("Log", size="4"),
        rx.html(State.log_messages),

        margin="2em"

    )
