"""State of the application."""
from typing import Any
import reflex as rx
import os
from datetime import datetime
import csv
from configparser import ConfigParser
from html2text import HTML2Text
import plotly.graph_objects as go
import PIL
import pandas as pd
from io import BytesIO
import re
import numpy as np

from diplomaapp.email_body import email_body
from diplomaapp.generate_diplomas import GenerateDiplomas

fonts_dir = "fonts"
csv_fields = ['Full name', 'Email', 'Id', 'Get Diploma']


def comma_name_to_full_name(name_string: str):
    """Transform name of form 'last, first' to 'first last'."""
    split_list = name_string.split(sep=',')
    return ' '.join([name.strip() for name in reversed(split_list)])


def extract_diploma_number(filename: str):
    """
    Extract the diploma number from a filename.

    Parameters
    ----------
    filename : str
        The filename.

    Returns
    -------
    str | int
        If found, then return number else 0.

    """
    match = re.search(r'-(\d+)\.', filename)
    if match:
        return int(match.group(1))
    else:
        return 0


class State(rx.State):
    """State of the app."""

    config_save_text: str = ""

    workshop_open_message: str
    workshop: str
    number: str
    workshop_code: str

    diploma_figure: go.Figure = go.Figure()
    diploma_figure_layout: dict[str, Any] = {}

    name_position_top: str = "1433"
    name_position_bot: str = "1852"
    text_name_y_position: str = "1642"
    fonts_available: list[str] = os.listdir(fonts_dir)
    font_write_diploma_name: str
    default_font_size_name: str

    include_dn_text: bool = True
    bottom_text: str
    dn_position_top: str = "2600"
    dn_position_bot: str = "2882"
    text_dn_y_position: str = "2741"
    font_write_diploma_1: str
    font_size_diploma_number: str

    diploma_number_line: str

    email_subject: str
    email_content_html: str

    participant_file_data: pd.DataFrame = \
        pd.DataFrame(data=[[' '], [' '], [' '], [' ']])
    data_columns: list[str]
    full_name_column: str
    first_name_column: str
    last_name_column: str
    last_name_first_name_column: str
    email_column: str
    id_column: str
    process_column: str
    participant_data_saved_text: str = ""
    participants: list[list[str]]
    select_all: bool = True
    participant_columns: list[dict[str, str]] = [
        {
            "title": "Full name",
            "type": "str",
            "width": 350,
        },
        {
            "title": "Email",
            "type": "str",
            "width": 350,
        },
        {
            "title": "Id",
            "type": "int",
            "width": 40,
        },
        {
            "title": "☐ Process",
            "type": "bool",
            "width": 100,
        }
    ]

    log_messages: str = ''

    test_email_address: str = ''

    diploma_gallery_image = PIL.Image.fromarray(np.array([[300]]), mode="L")
    currently_shown_diploma_img: str
    created_diploma_images_list: list[str]

    existing_workshops_list: list[str] = []

    _dir_base: str
    _dir_diploma_img: str
    _dir_diploma_pdf: str
    _config_file_path: str
    _log_file_path: str
    _participants_list_path: str
    _diploma_img_template_path: str
    _diploma_prefix_name: str

    @rx.event
    def reset_vars(self):
        """Reset all vars."""
        self.reset()
        self.create_workshop_list()

    @rx.event
    def setup_workshop_config(self, workshop_dict: dict):
        """
        Accept form data to create or load workshop data.

        Parameters
        ----------
        workshop_dict : dict
            Contains keys "workshop" (such as QBronze) and "workshop_number"

        """
        self.reset_vars()

        # additional reset actions
        self.email_content_html = ''

        # workshop
        self.workshop = workshop_dict['workshop']
        self.number = workshop_dict['workshop_number']
        other_name = workshop_dict["other_name"]

        if self.workshop == "Other":
            if len(other_name) > 0:
                self.workshop = other_name
            else:
                raise ValueError("Invalid input. Enter Other workshop's name.")

        self.workshop_code = self.workshop + self.number

        self._dir_base = os.path.join('Workshops', self.workshop_code)

        # make data dir
        os.makedirs(self._dir_base, exist_ok=True)

        # generated diploma dirs
        self._dir_diploma_img = os.path.join(self._dir_base, 'diploma_img')
        self._dir_diploma_pdf = os.path.join(self._dir_base, 'diploma_pdf')

        self._config_file_path = os.path.join(self._dir_base,
                                              'workshop.config')
        self._log_file_path = os.path.join(self._dir_base, 'output.log')

        # participants file
        self._participants_list_path = os.path.join(self._dir_base, 'data.csv')

        # diploma template
        self._diploma_img_template_path = os.path.join(self._dir_base,
                                                       'template.jpg')

        # if the config file exists, then load all the following from it
        # else set defaults.
        if os.path.exists(self._config_file_path):
            config = ConfigParser()
            config.read(self._config_file_path)
            c = config['DEFAULT']

            self._diploma_prefix_name = c['DIPLOMA_PREFIX_NAME']
            self.email_subject = c['EMAIL_SUBJECT']
            self.email_content_html = c['EMAIL_CONTENT_HTML']
            self.name_position_top = str(c['NAME_POSITION_TOP'])
            self.name_position_bot = str(c['NAME_POSITION_BOT'])
            self.text_name_y_position = str(c['TEXT_NAME_Y_POSITION'])
            self.font_write_diploma_name = os.path.basename(
                c['FONT_WRITE_DIPLOMA_NAME'])
            self.default_font_size_name = str(c['DEFAULT_FONT_SIZE_NAME'])
            # this is a new property; has to have a default value.
            if 'INCLUDE_DN_TEXT' in c:
                self.include_dn_text = c.getboolean('INCLUDE_DN_TEXT')
            else:
                self.include_dn_text = True
            # this is a new property; has to have a default value.
            if 'BOTTOM_TEXT' in c:
                self.bottom_text = str(c['BOTTOM_TEXT'])
            else:
                self.bottom_text = ''
            self.dn_position_top = str(c['DN_POSITION_TOP'])
            self.dn_position_bot = str(c['DN_POSITION_BOT'])
            self.text_dn_y_position = str(c['TEXT_DN_Y_POSITION'])
            self.font_write_diploma_1 = os.path.basename(
                c['FONT_WRITE_DIPLOMA_1'])
            self.font_size_diploma_number = str(c['FONT_SIZE_DIPLOMA_NUMBER'])
            self.diploma_number_line = c['TEXT_DIPLOMA_NUMBER']

            # Load the participant list
            self._participants_list_path = c['FILE_DATA_CSV']
            if os.path.exists(self._participants_list_path):
                self.load_participants()

            # create figure for template
            self.set_diploma_img_template_path(c['FILE_DIPLOMA_TEMPLATE'])

            # show existing generated diplomas
            self.create_created_diploma_images_list()

            self.workshop_open_message = 'Successfully loaded previous ' \
                + f'config for {self.workshop_code}. Proceed to next tab.'

        else:
            # these are all the workshop props user can change
            self._diploma_prefix_name = self.workshop_code + '-'

            # Text props
            self.name_position_top = "1433"
            self.name_position_bot = "1852"
            self.text_name_y_position = "1642"
            self.font_write_diploma_name = "RobotoCondensed-Light.ttf"
            self.default_font_size_name = "236"

            self.include_dn_text = True
            self.bottom_text = ""
            self.dn_position_top = "2600"
            self.dn_position_bot = "2882"
            self.text_dn_y_position = "2741"
            self.font_write_diploma_1 = "Lato-Regular.ttf"
            self.font_size_diploma_number = "50"
            self.set_diploma_number_line(
                f'Diploma Number: {self._diploma_prefix_name}')

            # set email subject and body
            self.set_email_subject(f'QWorld | {self.workshop_code} Diploma')
            self.set_email_content_html(email_body)

            self.workshop_open_message = 'Successfully generated new ' \
                + f'config for {self.workshop_code}. Proceed to next tab.'

            self.create_config(False, False)

    @rx.event
    async def handle_upload_diploma_template(
            self,
            files: list[rx.UploadFile]
    ):
        """
        Handle upload of diploma template.

        Parameters
        ----------
        files : list[rx.UploadFile]
            A single image file
        """
        file = files[0]

        upload_data = await file.read()

        ext = os.path.splitext(file.filename)[-1]
        file_path = os.path.join(self._dir_base, 'template' + ext)

        # Save the file.
        with open(file_path, "wb") as file_object:
            file_object.write(upload_data)

        self.set_diploma_img_template_path(file_path)

    @rx.event
    def set_diploma_img_template_path(self, val: str):
        """
        Set path to diploma template file, and create figure.

        Parameters
        ----------
        val : str
            Path of the file
        """
        self._diploma_img_template_path = val
        if os.path.isfile(self._diploma_img_template_path):
            self.create_figure()

    def create_figure(self):
        """Create the diploma template figure with lines on it."""
        canvas_width = 1000
        img = PIL.Image.open(self._diploma_img_template_path)
        ysize = img.height*canvas_width/img.width

        self.diploma_figure = go.Figure()

        self.diploma_figure.update_layout(
            xaxis=dict(range=[0, canvas_width],
                       showgrid=False,
                       showticklabels=False),
            yaxis=dict(range=[img.height, 0],
                       showgrid=False,
                       dtick=200)
        )

        self.diploma_figure.update_layout(width=canvas_width)
        self.diploma_figure.update_layout(height=ysize)

        # name top
        self.diploma_figure.add_shape(
            type='line',
            x0=0, x1=canvas_width,
            y0=self.name_position_top, y1=self.name_position_top,
            line=dict(color='red', width=2)
        )

        self.diploma_figure.add_shape(
            type='line',
            x0=0, x1=canvas_width,
            y0=self.name_position_bot, y1=self.name_position_bot,
            line=dict(color='blue', width=2)
        )

        self.diploma_figure.add_shape(
            type='line',
            x0=0, x1=canvas_width,
            y0=self.dn_position_top, y1=self.dn_position_top,
            line=dict(color='magenta', width=2)
        )

        self.diploma_figure.add_shape(
            type='line',
            x0=0, x1=canvas_width,
            y0=self.dn_position_bot, y1=self.dn_position_bot,
            line=dict(color='goldenrod', width=2)
        )

        self.diploma_figure.add_layout_image(
            dict(
                source=img,
                xref="x",
                yref="y",
                x=0,
                y=0,
                sizex=canvas_width,
                sizey=img.height,
                sizing="fill",
                layer="below")
        )

        self.diploma_figure_layout = self.diploma_figure.to_dict()["layout"]

    @rx.event
    async def handle_upload_participant_list(
            self,
            files: list[rx.UploadFile]
    ):
        """
        Handle upload of participant list.

        Parameters
        ----------
        files : list[rx.UploadFile]
            A csv file.
        """
        file = files[0]
        upload_data = await file.read()

        # load data and process
        try:
            self.participant_file_data = pd.read_csv(BytesIO(upload_data),
                                                     dtype=str,
                                                     header=None,
                                                     keep_default_na=False)
        except:
            self.participant_file_data = pd.read_excel(BytesIO(upload_data),
                                                       dtype=str,
                                                       header=None,
                                                       keep_default_na=False)

        nrows, ncols = self.participant_file_data.shape
        self.data_columns = ["-"] + [chr(x) for x in range(65, 65+ncols)]
        self.participant_file_data.columns = self.data_columns[1:]

        self.participant_file_data.insert(0,
                                          "row",
                                          list(range(1, nrows+1)))
        self.full_name_column = "-"
        self.last_name_first_name_column = "-"
        self.first_name_column = "-"
        self.last_name_column = "-"
        self.email_column = "-"
        self.id_column = "-"
        self.process_column = "-"

    @rx.event
    def delete_data_row(self):
        """Delete top data row."""
        row_ind = self.participant_file_data.index.values[0]
        self.participant_file_data = self.participant_file_data.drop(row_ind)

    @rx.event
    def apply_column_selection(self):
        """Use user selected columns to create data table."""
        # get the names
        if self.full_name_column != "-":
            full_names = self.participant_file_data[[self.full_name_column]]
        elif self.last_name_first_name_column != "-":
            full_names = self.participant_file_data[
                [self.last_name_first_name_column]]
            full_names = full_names.map(comma_name_to_full_name)
        elif self.first_name_column != "-" and self.last_name_column != "-":
            full_names = self.participant_file_data[self.first_name_column] \
                + ' ' + self.participant_file_data[self.last_name_column]
        else:
            return

        # clean up spaces
        full_names = full_names.map(str.strip)

        if self.email_column != "-":
            emails = self.participant_file_data[[self.email_column]]
            emails = emails.map(str.strip)
        else:
            return

        nrows = full_names.shape[0]

        if self.id_column != "-":
            ids = self.participant_file_data[[self.id_column]]
        else:
            ids = pd.DataFrame(range(1, nrows+1), dtype=str)

        if self.process_column != "-":
            process = self.participant_file_data[[self.process_column]]
        else:
            process = pd.DataFrame([1]*nrows, dtype=str)

        self.participant_file_data = \
            pd.concat([full_names.reset_index(drop=True),
                       emails.reset_index(drop=True),
                       ids.reset_index(drop=True),
                       process.reset_index(drop=True)],
                      axis=1)
        self.participant_file_data.columns = csv_fields

    @rx.event
    def save_processed_participant_data(self):
        """Save processed participant data."""
        self.participant_file_data.to_csv(self._participants_list_path,
                                          index=False)

        # clear all vars
        self.full_name_column = ''
        self.last_name_first_name_column = ''
        self.first_name_column = ''
        self.last_name_column = ''
        self.email_column = ''
        self.id_column = ''
        self.process_column = ''
        self.participant_file_data = \
            pd.DataFrame(data=[[' '], [' '], [' '], [' ']])

        # load data onto next tab
        self.load_participants()
        self.participant_data_saved_text = "Saved. Proceed to next tab."

    def load_participants(self):
        """Process the csv file of participants."""
        # now display
        with open(self._participants_list_path, encoding='utf-8') as csvfile:
            reader = csv.reader(csvfile)

            # skip header
            next(reader)
            self.participants = list(reader)

            for i in range(len(self.participants)):
                self.participants[i][3] = bool(int(self.participants[i][3]))

            if all([par[3] for par in self.participants]):
                self.select_all = True
                self.participant_columns[3]['title'] = "☑ Process"

    @rx.event
    def set_participant_details(self,
                                pos,
                                val):
        """
        Set participant details.

        Parameters
        ----------
        pos : tuple(int)
            cell [row, col].
        val : dict[str, Any]
            The value of the cell.
        """
        self.participants[pos[1]][pos[0]] = val['data']

        # process participant
        if pos[0] == 3:
            # if any participant is deselected
            if not val['data']:
                self.select_all = False
                self.participant_columns[3]['title'] = "☐ Process"
            # if a participant is selected and all are selected
            if val['data'] and all(self.participants[3]):
                self.select_all = True
                self.participant_columns[3]['title'] = "☑ Process"

    @rx.event
    def header_clicked(self, index):
        """
        Set whether all participants are selected/deselected.

        Parameters
        ----------
        index : int
            index of column head being clicked.
        """
        if index != 3:
            return

        if not self.select_all:
            self.select_all = True
            self.participant_columns[3]['title'] = "☑ Process"
            for i in range(len(self.participants)):
                self.participants[i][3] = True
        else:
            self.select_all = False
            self.participant_columns[3]['title'] = "☐ Process"
            for i in range(len(self.participants)):
                self.participants[i][3] = False

    @rx.event
    def add_participant(self):
        """Add a participant."""
        self.participants.append(['', '', str(len(self.participants)+1), True])

    @rx.event
    def handle_email_editor_change(self, content: str):
        """
        Update when email template content changes.

        Parameters
        ----------
        content : str
            The email template content.
        """
        self.email_content_html = content

    @rx.event
    def download_participant_data(self):
        """Prepare file to download participant data."""
        return rx.download(data=self._participants_list_path,
                           filename="data.csv")

    def html_email_to_text(self):
        """Convert email html to text."""
        # convert html to text
        h = HTML2Text()
        h.body_width = 80
        h.protect_links = True
        h.inline_links = True
        return h.handle(self.email_content_html)

    def write_test_participant_list(self, path: str):
        """
        Write participant data list.

        Parameters
        ----------
        path : str
            Path to file to write.
        """
        data = [
            ["Full name", "Email", "Id", "Get Diploma"],
            ["Muhammad Lee", self.test_email_address, "1", "1"]
        ]

        with open(path, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerows(data)

    @rx.event
    def create_config(self,
                      bool_diploma_generation: bool,
                      bool_send_email: bool,
                      create_test_config: bool = False
                      ):
        """
        Create and write config.

        Parameters
        ----------
        bool_diploma_generation : bool
            Whether diploma images/pdf are to be generated.
        bool_send_email : bool
            Whether dipomas are sent via email.
        create_test_config : bool, optional
            For sending test email. The default is False.
        """
        config = ConfigParser()
        if os.path.exists(self._config_file_path):
            config.read(self._config_file_path)
        else:
            config.read('config_file/workshop.config')

        config_file_path = self._config_file_path

        c = config['DEFAULT']

        c['FILE_DATA_CSV'] = self._participants_list_path
        if create_test_config:
            c['FILE_DATA_CSV'] = os.path.join(self._dir_base, 'test.csv')
            config_file_path = os.path.join(self._dir_base,
                                            'test_workshop.config')
        c['FILE_DIPLOMA_TEMPLATE'] = self._diploma_img_template_path
        c['DIPLOMA_SAVE_PATH'] = self._dir_diploma_img
        c['DIPLOMA_PDF_SAVE_PATH'] = self._dir_diploma_pdf
        c['DIPLOMA_GENERATION'] = str(bool_diploma_generation)
        c['EMAIL_SENDING'] = str(bool_send_email)
        c['DIPLOMA_PREFIX_NAME'] = self._diploma_prefix_name
        c['NAME_POSITION_TOP'] = str(self.name_position_top)
        c['NAME_POSITION_BOT'] = str(self.name_position_bot)
        c['TEXT_NAME_Y_POSITION'] = str(self.text_name_y_position)
        c['FONT_WRITE_DIPLOMA_NAME'] = os.path.join(
                                            fonts_dir,
                                            self.font_write_diploma_name)
        c['DEFAULT_FONT_SIZE_NAME'] = str(self.default_font_size_name)
        c['INCLUDE_DN_TEXT'] = str(self.include_dn_text)
        c['BOTTOM_TEXT'] = self.bottom_text
        c['DN_POSITION_TOP'] = str(self.dn_position_top)
        c['DN_POSITION_BOT'] = str(self.dn_position_bot)
        c['TEXT_DN_Y_POSITION'] = str(self.text_dn_y_position)
        c['FONT_WRITE_DIPLOMA_1'] = os.path.join(
                                        fonts_dir,
                                        self.font_write_diploma_1)
        c['FONT_SIZE_DIPLOMA_NUMBER'] = str(self.font_size_diploma_number)
        c['TEXT_DIPLOMA_NUMBER'] = self.diploma_number_line
        c['EMAIL_SUBJECT'] = self.email_subject
        c['EMAIL_CONTENT_HTML'] = self.email_content_html.format(
            name='{name}',
            workshop_code=self.workshop_code,
            workshop=self.workshop)
        c['EMAIL_CONTENT_TEXT'] = self.html_email_to_text().format(
            name='{name}',
            workshop_code=self.workshop_code,
            workshop=self.workshop)

        with open(config_file_path, 'w') as file:
            config.write(file)

        if create_test_config:
            self.write_test_participant_list(c['FILE_DATA_CSV'])
        else:
            # write participants file
            part_list = []
            for par in self.participants:
                part_list.append(par[0:3] + ['1' if par[3] else '0'])

            with open(self._participants_list_path, 'w') as f:
                write = csv.writer(f)

                write.writerow(csv_fields)
                write.writerows(part_list)

        self.create_workshop_list()
        now = datetime.now()
        self.config_save_text = "Config saved at " + now.strftime("%H:%M:%S")

    @rx.event
    def create_diplomas(self):
        """Create diplomas images/pdf."""
        self.create_config(True, False)

        gd = GenerateDiplomas(self._config_file_path, self._log_file_path)
        gd.generate()
        for log_msg in gd.generate():
            self.log_messages += '<br />' + log_msg
            yield

        self.create_created_diploma_images_list()

    @rx.event
    def send_emails(self):
        """Send already created diplomas as email."""
        self.create_config(False, True)

        gd = GenerateDiplomas(self._config_file_path, self._log_file_path)
        gd.generate()
        for log_msg in gd.generate():
            self.log_messages += '<br />' + log_msg
            yield

    @rx.event
    def send_test_email(self):
        """Send a test email."""
        test_config_file_path = os.path.join(self._dir_base,
                                             'test_workshop.config')
        self.create_config(True, True, create_test_config=True)

        gd = GenerateDiplomas(test_config_file_path, self._log_file_path)
        gd.generate()
        for log_msg in gd.generate():
            self.log_messages += '<br />' + log_msg
            yield

    @rx.event
    def set_name_position_top(self, val: str):
        """
        Set y position for line determining top of name bounding box.

        Parameters
        ----------
        val : str
            y position. Should be numeric.
        """
        if val.isnumeric():
            self.name_position_top = str(int(float(val)))

            self.text_name_y_position = str(int(
                (int(self.name_position_top) + int(self.name_position_bot))/2))

            self.create_figure()

    @rx.event
    def set_name_position_bot(self, val: str):
        """
        Set y position for line determining bot of name bounding box.

        Parameters
        ----------
        val : str
            y position. Should be numeric.
        """
        if val.isnumeric():
            self.name_position_bot = str(int(float(val)))

            self.text_name_y_position = str(int(
                (int(self.name_position_top) + int(self.name_position_bot))/2))

            self.create_figure()

    @rx.event
    def set_dn_position_top(self, val: str):
        """
        Set y position for line determining top of diploma number bounding box.

        Parameters
        ----------
        val : str
            y position. Should be numeric.
        """
        if val.isnumeric():
            self.dn_position_top = str(int(float(val)))

            self.text_dn_y_position = str(int(
                (int(self.dn_position_top) + int(self.dn_position_bot))/2))

            self.create_figure()

    @rx.event
    def set_dn_position_bot(self, val: str):
        """
        Set y position for line determining bot of diploma number bounding box.

        Parameters
        ----------
        val : str
            y position. Should be numeric.
        """
        if val.isnumeric():
            self.dn_position_bot = str(int(float(val)))

            self.text_dn_y_position = str(int(
                (int(self.dn_position_top) + int(self.dn_position_bot))/2))

            self.create_figure()

    def create_created_diploma_images_list(self):
        """Create a list of image files in the diploma img folder."""
        if not os.path.exists(self._dir_diploma_img):
            return

        self.created_diploma_images_list = \
            sorted(os.listdir(self._dir_diploma_img),
                   key=extract_diploma_number)
        if len(self.created_diploma_images_list) > 0:
            self.set_currently_shown_diploma_image(
                self.created_diploma_images_list[0])

    rx.event
    def set_currently_shown_diploma_image(self, filename: str):
        """
        Set the currently shown diploma image.

        Parameters
        ----------
        filename : str
            The name of the file to show.
        """
        self.currently_shown_diploma_img = filename
        img_path = os.path.join(self._dir_diploma_img, filename)
        self.diploma_gallery_image = PIL.Image.open(img_path)

    @rx.event
    def create_workshop_list(self):
        """Create list of workshops saved in Workshops folder."""
        path = "Workshops"
        if not os.path.exists(path):
            return

        file_list = sorted(os.listdir(path),
                           key=lambda x: os.path.getctime(
                               os.path.join(path, x)),
                           reverse=True)

        self.existing_workshops_list = \
            [file for file in file_list if file[0] != "."]
