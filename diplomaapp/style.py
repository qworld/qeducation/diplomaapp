import reflex as rx


Style = {
    ".html_render": {
        "margin": "2em",
    },

    rx.tabs.trigger: {
        "width": "12%",
        "font_size": "1.5em"
    },

    rx.tabs.list: {
        "margin-left": "3%",
        "margin-right": "3%",
        "margin-bottom": "2em",
    },

    rx.hstack: {
        "margin-bottom": "10px"
    },

    rx.heading: {
        "align": "center"}

}
