import reflex as rx

from diplomaapp.state import State

form_label_style = {
    "width": '15em',
    "text_align": "right",
    "margin-right": "1em",
    "margin-bottom": "5px"
}

form_item_style = {
    "margin-bottom": "8px"
}

def render_list_item(item: rx.Var[str]):
    """Render a single item."""
    return rx.list.item(item)


def workshop_details() -> rx.Component:
    return rx.center(
        rx.vstack(
            rx.form.root(
                rx.vstack(

                    rx.form.field(
                        rx.hstack(
                            rx.form.label('Workshop type*:',
                                          style=form_label_style
                                          ),
                            rx.select(
                                ['QPrep',
                                 'QBronze',
                                 'QCobalt',
                                 'QMercury',
                                 'QNickel',
                                 'QSilver',
                                 'QZinc',
                                 'Quantum with String Diagrams',
                                 'Other'
                                 ],
                                placeholder='Workshop type',
                                name="workshop",
                                width="15em",
                                style=form_item_style
                            ),
                            rx.form.label("Other:",
                                          width="7em",
                                          style=form_label_style),
                            rx.input(
                                name="other_name",
                                type="text",
                                width="15em",
                                style=form_item_style)
                        )),

                    rx.form.field(
                        rx.hstack(
                            rx.form.label('Workshop number*:',
                                          style=form_label_style),
                            rx.input(
                                name='workshop_number',
                                type="number",
                                width="5em",
                                style=form_item_style),
                        )),

                    rx.form.submit(
                        rx.button('Generate or open previous'),
                        as_child=True)
                ),

                on_submit=State.setup_workshop_config,
                margin_bottom="2em"
            ),


            rx.text(State.workshop_open_message),

            rx.heading("Existing workshops",
                       size="5"),
            rx.foreach(State.existing_workshops_list,
                       render_list_item)
        ),
        margin="2em"
    )
