This is a software that can be used to generate diplomas, suitable for QWorld's basic workshop needs.

It can either be used on a local computer, or installed on a server.

# Installation and usage on local computer.
Clone this repository. Then inside it, create a python virtual environment, and activate it.

```bash
python -m venv env
source env/bin/activate
```

Then install necessary packages

```bash
python -m pip install -r requirements.txt
```

1. Rename `config_file/example_workshop.config` to `config_file/workshop.config`. Similarly for `config_file/example_auth.config`. Fill out email details (user name, password etc) in the config file. 

Then run

```bash
reflex run
```

Open the resultant url in your browser and follow the steps.

# Installation on server
We can create an installation on a cloud server, such as one obtained from Hetzner or similar service. 

1. Obtain the latest Debian server.

2. Login using ssh. If you can't, then first use the console in the web interface to login as root. Then ensure ssh server is installed and running.

```bash
sudo apt-get install openssh-server
sudo systemctl status ssh
```

3. Create a new user for the service
```bash
sudo useradd diploma
sudo usermod -aG sudo diploma
```

4. Login via ssh into user `diploma`.

5. Upgrade and install necessary packages

```bash
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install git python-is-python3 python3.11-venv unzip passlib nginx certbot python3-certbot-nginx ufw
```

6. Clone the repo
```bash
git clone https://gitlab.com/qworld/qeducation/diplomaapp.git
cd diploma app
```

7. Create virtual environment and activate
```bash
python -m venv .venv
echo "*" > .venv/.gitignore
source .venv/bin/activate
```

8. Install requirements for reflex
```bash
python -m pip install -r requirements.txt 
```

9. Rename config files.

```bash
mv config_file/example_workshop.config config_file/workshop.config
mv config_file/example_auth.config config_file/auth.config
```

Edit these files carefully, adding necessary usernames and passwords.

10. Add url to rxconfig.

```bash
nano rxconfig.py 
```

And ensure you have

```python
config = rx.Config(
    app_name="diplomaapp",
    telemetry_enabled=False,
    api_url="https://diplomagenerator.qworld.net",
)
```

11. Time to start configuring nginx. First we create the configuration file.

```bash
sudo nano /etc/nginx/sites-available/diplomagenerator.qworld.net
```

Paste these contents.

```
server {
    listen 80;
    server_name diplomagenerator.qworld.net;
    return 301 https://$server_name$request_uri;
}

server {
    listen 443 ssl;
    server_name diplomagenerator.qworld.net;

    ssl_certificate /etc/letsencrypt/live/diplomagenerator.qworld.net/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/diplomagenerator.qworld.net/privkey.pem;

    # Frontend
    location / {
        proxy_pass http://localhost:3000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    # Backend
    location /_event {
        proxy_pass http://localhost:8000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    location /ping {
        proxy_pass http://localhost:8000;
    }

    location /_upload {
        proxy_pass http://localhost:8000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;

        client_max_body_size 30M;
    }

}
```

Make it the active policy

```bash
sudo ln -s /etc/nginx/sites-available/diplomagenerator.qworld.net /etc/nginx/sites-enabled/diplomagenerator.qworld.net
sudo rm /etc/nginx/sites-enabled/default
```

12. If you ever change nginx config, be sure to restart it.

```bash
sudo systemctl restart nginx
```

You can also check its status

```bash
sudo systemctl status nginx
```

13. Install certbot for https

```bash
sudo certbot --nginx -d diplomagenerator.qworld.net
```

14. Use a firewall

```
sudo ufw enable
sudo ufw allow 22
sudo ufw allow 80
sudo ufw allow 443
sudo ufw allow 25
sudo ufw allow 3000
sudo ufw allow 8000
sudo ufw status
```

15. Now create a systemd service

Open

```bash
sudo nano /etc/systemd/system/reflex.service
```

And set it to the following. You can discover the PATH by just doing `echo $PATH`.

```                                                                     
[Unit]
Description=Reflex Web Application
After=network.target

[Service]
Type=simple
User=diploma
WorkingDirectory=/home/diploma/diplomaapp
Environment="PATH=/home/diploma/diplomaapp/.venv/bin:/home/diploma/.local/share/reflex/bun/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games" 
ExecStart=/home/diploma/diplomaapp/.venv/bin/reflex run --env prod
Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target
```

Start it.

```bash
sudo systemctl enable reflex
sudo systemctl start reflex
```

16. You are done. Point your browser at [https://diplomagenerator.qworld.net/](https://diplomagenerator.qworld.net/) and enjoy.

# Acknowledgment

This uses code for [diploma generation](https://gitlab.com/qworld/qeducation/diploma-generator), initially written by Claudia Zendejas-Morales.
